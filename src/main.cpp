#include <Arduino.h>

#define D3 3
#define D4 4
#define D5 5

void setup(void) {
  pinMode(D3, OUTPUT);
  pinMode(D4, OUTPUT);
  pinMode(D5, OUTPUT);
}

void loop(void) {
  digitalWrite(D3, 1);
  delay(1000);
  digitalWrite(D3, 0);

  digitalWrite(D4, 1);
  delay(1000);
  digitalWrite(D4, 0);

  digitalWrite(D5, 1);
  delay(1000);
  digitalWrite(D5, 0);
}
