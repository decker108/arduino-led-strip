# Example Arduino Nano and LED-strip project

This project combines an RGB LED-strip, an Arduino Nano, three transistors, 
three resistors and a battery to create a circuit that toggles the LED's
based on a pre-programmed pattern.

The program could be extended to take a pattern from another source, such as
a message received over the serial port or a peripheral communication device
using WiFi, Bluetooth or other technologies.

Pins used:
* GND
* D3
* D4
* D5

Components:
* Arduino Nano (nano atmega 328) 
* RGB LED strip (12V)
* 3 IRLZ44N transistors
* 3 4.7k Ohm resistors
* 9V-12V power source (the Arduino Nano cannot power the LED strip alone)

![Circuit board](/images/board.jpg) 